package me.b4rt3w.drop.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import me.b4rt3w.drop.managers.Drop;
import me.b4rt3w.drop.managers.DropData;

public class PlayerListener implements Listener{
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		double num = Math.random();
		
		Drop drop = randomDrop(DropData.getAllDrops());
		Bukkit.broadcastMessage("Randomowy drop to : " + drop.getName());
		
		
		
	}
	

	public static void main(String[] args) {
		double chance = 10/100;
		System.out.print("Randomowa liczba to: " + chance);
	}
	
	private static final Random random = new Random();

	private Drop randomDrop(Drop[] array) {
		
		List<Drop> ableToChoose = new ArrayList<>();
		double power = Math.random() * 100;
		for(Drop drop : array) {
			if(drop.getChance() <= power) ableToChoose.add(drop);
		}
		if(ableToChoose.isEmpty()) return null;
		
		return ableToChoose.get(random.nextInt(ableToChoose.size()));
	}
}
