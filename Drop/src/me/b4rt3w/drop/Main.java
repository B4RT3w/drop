package me.b4rt3w.drop;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.b4rt3w.drop.commands.DropCMD;
import me.b4rt3w.drop.listeners.PlayerListener;
import me.b4rt3w.drop.managers.ConfigManager;
import me.b4rt3w.drop.managers.Drop;
import me.b4rt3w.drop.managers.DropData;
import me.b4rt3w.drop.yaml.CraftYamlConfig;
import me.b4rt3w.drop.yaml.YamlConfig;

public class Main extends JavaPlugin{
	
	private static Main inst;
	private ConfigManager cm;
	private static YamlConfig cfg;
	
	public static YamlConfig getCfg() {
		return cfg;
	}
	
	public static Main getInstance() {
		return inst;
	}
	
	public void onEnable() {
		inst = this;
		cfg = CraftYamlConfig.asConfig(new File(getDataFolder(), "config.yml"));
		cm = new ConfigManager(this);
		cm.createConfig();
		registerAllDrops();
		getCommand("drop").setExecutor(new DropCMD());
		registerListeners();
	}
	
	public void onDisable() {
		
	}
	
	public void registerAllDrops() {
			for(String s : getCfg().getConfig().getConfigurationSection("drops").getKeys(false)) {
				Drop d = new Drop(s , getCfg().getConfig().getString("drops." + s + ".display-name") , getCfg().getConfig().getDouble("drops." + s + ".chance"), Material.getMaterial(getCfg().getConfig().getString("drops." + s + ".material")), getCfg().getConfig().getStringList("drops." + s + ".lore"), getCfg().getConfig().getString("drops." + s + ".amount"));
				DropData.addDrop(d);
			}
	}
	
	private void registerListeners() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new PlayerListener(), this);
	}

}
