package me.b4rt3w.drop.managers;

import java.util.concurrent.CopyOnWriteArrayList;

public class DropData {

	private static volatile CopyOnWriteArrayList<Drop> drops = new CopyOnWriteArrayList<>();

	public static Drop[] getAllDrops() {
		return drops.toArray(new Drop[drops.size()]);
	}

	public static void addDrop(Drop drop) {
		drops.addIfAbsent(drop);
	}

	public static void removeDrop(Drop drop) {
		drops.remove(drop);
	}

}
