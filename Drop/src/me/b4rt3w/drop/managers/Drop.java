package me.b4rt3w.drop.managers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;

public class Drop {
	
	private String displayName;
	private String name;
	private double chance;
	private Material mat;
	private List<String> lore;
	private String amount;
	
	public Drop(String namex, String disp, double chan, Material material, List<String> lor, String amount) {
		this.name = namex;
		this.displayName = disp;
		this.chance = chan;
		this.mat = material;
		this.lore = lor;
		this.amount = amount; 
	}
	
	public Drop() {
	}
	
	public List<String> getLore() {
		return new ArrayList<>(lore);
	}

	public String getAmount() {
		return amount;
	}


	
	public String getDisplayName() {
		return displayName;
	}

	public double getChance() {
		return chance;
	}
	
	public String getName() {
		return name;
	}

	public Material getMat() {
		return mat;
	}

	

}
