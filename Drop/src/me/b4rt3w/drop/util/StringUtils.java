package me.b4rt3w.drop.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;

public class StringUtils {
	
	public static String fixColor(String string) {
		if (string == null) {
			return "";
		}
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	public static List<String> fixColor(List<String> s) {
		List<String> colored = new ArrayList<String>();
		for (String str : s) {
			colored.add(fixColor(str));
		}
		return colored;
	}
	
	 public static String secToMin(int i)
	 {
	 int ms = i / 60;
	 int ss = i % 60;
	 String m = (ms < 10 ? "0" : "") + ms;
	 String s = (ss < 10 ? "0" : "") + ss;
	 String f = m + ":" + s;
	 return f;
	 }

}
