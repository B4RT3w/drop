package me.b4rt3w.drop.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.b4rt3w.drop.Main;
import me.b4rt3w.drop.managers.Drop;
import me.b4rt3w.drop.managers.DropData;
import me.b4rt3w.drop.util.ItemCreator;
import me.b4rt3w.drop.util.StringUtils;

public class DropCMD implements CommandExecutor{

	private static List<ItemStack> itemStacks = new ArrayList<>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("drop")) {
			if(sender instanceof Player) {
				Player p = (Player)sender;
				openMenu(p);
				p.sendMessage("test");
			} else {
				//TO DO
			}
		}
		return false;
	}
	
	public static void openMenu(Player p) {
		p.openInventory(fillInv(Bukkit.getServer().createInventory(p, 27, "�eDropy"), p));
	}
	
	public static Inventory fillInv(Inventory inv, Player p) {
		

		for(int i = 0; i < DropData.getAllDrops().length; i++) {
			Drop drop = DropData.getAllDrops()[i];
			Material mat = Material.getMaterial(Main.getCfg().getConfig().getString("drops." + drop.getName() + ".material").toUpperCase());
			ItemStack item = new ItemStack(mat);
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(StringUtils.fixColor(drop.getDisplayName()));
			itemMeta.setLore(drop.getLore());
			item.setItemMeta(itemMeta);
			itemStacks.add(item);
			inv.setItem(i, item);
		}

		return inv;
	}
	
	

}
