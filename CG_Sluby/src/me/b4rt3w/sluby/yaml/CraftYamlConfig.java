package me.b4rt3w.sluby.yaml;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public final class CraftYamlConfig implements YamlConfig {

	public static HashMap<String, FileConfiguration> data = new HashMap<>();
	private File f;

	public static YamlConfig asConfig(File f) {
		return new CraftYamlConfig(f);
	}

	CraftYamlConfig(File f) {
		this.f = f;
		if (!data.containsKey(f.getAbsolutePath()))
			data.put(f.getAbsolutePath(),
					(FileConfiguration) YamlConfiguration.loadConfiguration(f));
	}

	@Override
	public FileConfiguration getConfig() {
		return data.get(f.getAbsolutePath());
	}

	@Override
	public void save() {
		try {
			data.get(f.getAbsolutePath()).save(f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void reload() {
		data.remove(f.getAbsolutePath());
		data.put(f.getAbsolutePath(),
				(FileConfiguration) YamlConfiguration.loadConfiguration(f));
	}

	public static class CraftMessages implements YamlConfig.Messages {

		private Map<String, String> messages;
		private YamlConfig cfg;
		private boolean saved;
		private boolean loaded;

		public static CraftMessages asMessagesConfig(YamlConfig cfg) {
			return new CraftMessages(cfg);
		}
		
		CraftMessages(YamlConfig cfg) {
			this.messages = new HashMap<>();
			this.cfg = cfg;
		}

		@Override
		public String getMessage(String message) {
			String m = messages.get(message);
			return m;
		}

		@Override
		public boolean isSaved() {
			return saved;
		}

		@Override
		public boolean isLoaded() {
			return loaded;
		}

		@Override
		public void setMessage(String message, String value) {
			messages.put(message, value);
		}

		@Override
		public void fetch() {
			for(String s : cfg.getConfig().getConfigurationSection("messages").getKeys(false)) {
				messages.put(s, cfg.getConfig().getString("messages." + s));
				System.out.println(s);
				
			}
			System.out.println(messages.size());
			loaded = true;
		}

		public void save() {
			for(String s : messages.keySet()) {
				cfg.getConfig().set("messages." + s, messages.get(s));
			}
			cfg.save();
			loaded = true;
			saved = true;
		}
	}
}