package me.b4rt3w.sluby.yaml;

import org.bukkit.configuration.file.FileConfiguration;

public interface YamlConfig {

	public FileConfiguration getConfig();

	public void save();

	public void reload();

	public interface Messages {

		public String getMessage(String message);

		public boolean isSaved();

		public boolean isLoaded();

		public void setMessage(String message, String value);

		public void fetch();

		public void save();
	}
}