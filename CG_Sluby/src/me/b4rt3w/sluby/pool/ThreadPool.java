package me.b4rt3w.sluby.pool;

import java.util.concurrent.ExecutorService;

public class ThreadPool {
	
	private static ExecutorService service = new CachedThreadPoolErrorReporting();
	
	public static void run(Runnable runnable) {
		service.execute(runnable);
	}

}
