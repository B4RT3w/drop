package me.b4rt3w.sluby;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;

import me.b4rt3w.sluby.commands.SlubyCMD;
import me.b4rt3w.sluby.listeners.PlayerListener;
import me.b4rt3w.sluby.managers.ConfigManager;
import me.b4rt3w.sluby.managers.MenuManager;
import me.b4rt3w.sluby.mysql.MySQL;
import me.b4rt3w.sluby.yaml.CraftYamlConfig;
import me.b4rt3w.sluby.yaml.YamlConfig;

public class Main extends JavaPlugin{
	
	private static Main inst;
	private static YamlConfig cfg;
	private static YamlConfig.Messages msgCfg;
	private static YamlConfig locCfg;
	private static MySQL mysql;
	private ConfigManager cm;
	private static Map<String, MarriagePlayer> players = new HashMap<>();
	
	public static MySQL getMySQL() {
		return mysql;
	}
	
	public static Main getInstance() {
		return inst;
	}
	
	public static YamlConfig getCfg() {
		return cfg;
	}

	public static YamlConfig getLocCfg() {
		return locCfg;
	}

	public static YamlConfig.Messages getMsgCfg() {
		return msgCfg;
	}
	
	public static void addPlayer(MarriagePlayer player) {
		players.put(player.getName(), player);
	}
	
	public static void removePlayer(String name) {
		players.remove(name);
	}
	
	public static MarriagePlayer getPlayer(String name) {
		return players.get(name);
	}
	
	public void onEnable() {
		inst = this;
		Bukkit.getConsoleSender().sendMessage("�ePlugin CG Sluby zostal wlaczony.");
		cfg = CraftYamlConfig.asConfig(new File(getDataFolder(), "config.yml"));
		msgCfg = CraftYamlConfig.CraftMessages.asMessagesConfig(CraftYamlConfig.asConfig(new File(getDataFolder(), "messages.yml")));
		locCfg = CraftYamlConfig.asConfig(new File(getDataFolder(), "locations.yml"));
		cm = new ConfigManager(this);
		cm.createConfig();
		cm.loadMessages();
		mysql = new MySQL();
		mysql.connect();
		mysql.getHandle().createTablesAsync();
		
		ProtocolLibrary.getProtocolManager().addPacketListener(
	            new PacketAdapter(Main.getInstance(), PacketType.Play.Client.USE_ENTITY) {
	                @Override
	                public void onPacketReceiving(PacketEvent event) {
	                        try {
	                            //Entity entity = event.getPacket().getEntityModifier(event).read(0);
	                            if(getID(event.getPacket()) == SlubyCMD.id) {
	                            	MenuManager.openMenu(event.getPlayer());
	                            }
	                        } catch (Exception e){
	                            
	                        }         
	                }
	            });
		registerCommands();
		registerListeners();
	}
	
	private int getID(PacketContainer packet) {
		int id = packet.getIntegers().read(0);
        if (id < 0){
        	 return 0;
        }  
        return id;
	}
	
	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage("�ePlugin CG Sluby zostal wylaczony.");
	}
	
	public void registerCommands() {
		getCommand("sluby").setExecutor(new SlubyCMD());
	}
	
	private void registerListeners() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new PlayerListener(), this);
	}
	
	
	

}
