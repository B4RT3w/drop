package me.b4rt3w.sluby.commands;

import me.b4rt3w.sluby.managers.ConfigManager;
import me.b4rt3w.sluby.packets.WrapperPlayServerEntityHeadRotation;
import me.b4rt3w.sluby.packets.WrapperPlayServerNamedEntitySpawn;
import me.b4rt3w.sluby.utils.StringUtils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedGameProfile;

public class SlubyCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("sluby")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				if (p.hasPermission("sluby.admin")) {
					if (args.length == 0) {
						p.sendMessage(StringUtils.fixColor("&e==== &dSluby &e===="));
						p.sendMessage(StringUtils.fixColor("&e/sluby ustaw npc"));
						return true;
					}
					if(args.length != 2) {
						p.sendMessage(StringUtils.fixColor("&e==== &dSluby &e===="));
						p.sendMessage(StringUtils.fixColor("&e/sluby ustaw npc"));
						return true;
					}
					if(args.length == 2 && args[0].equalsIgnoreCase("ustaw") && args[1].equalsIgnoreCase("npc")) {
						createNewNPC(p);
						p.sendMessage("Stworzono");
					} else {
						p.sendMessage(StringUtils.fixColor("&e==== &dSluby &e===="));
						p.sendMessage(StringUtils.fixColor("&e/sluby ustaw npc"));
						return true;
					}
				} else {
					p.sendMessage(StringUtils.fixColor(ConfigManager.getPrefix() + "&cNie posiadasz uprawnien do tej komendy."));
				}
			} else {
				sender.sendMessage("Musisz byc Graczem aby to zrobic :c");
			}
		}
		return false;
	}
	
	public static int id = 78216;
	
	//TO DO
	public void createNewNPC(Player player) {
		 WrapperPlayServerNamedEntitySpawn spawned = new WrapperPlayServerNamedEntitySpawn();
	     WrapperPlayServerEntityHeadRotation test = new WrapperPlayServerEntityHeadRotation();
	     	
	     @SuppressWarnings("deprecation")
		WrappedGameProfile profile = new WrappedGameProfile("d4a49a8791f54074af5e8f434c749193", "Sluby");
	     	
	     	test.setEntityId(215178);
	        spawned.setEntityID(id);
	        spawned.setPosition(player.getLocation().toVector());
	        spawned.setPlayerName("Sluby");
	        spawned.setProfile(profile);
	        
	        // The rotation of the player's head (in degrees)
	        spawned.setPitch(player.getLocation().getPitch() * 256.0F / 360.0F);
	        test.setHeadYaw(player.getLocation().getYaw());
	        
	        // Documentation: 
	        // http://mc.kev009.com/Entities#Entity_Metadata_Format
	        WrappedDataWatcher watcher = new WrappedDataWatcher();
	        watcher.setObject(0, (byte) 0);
	        spawned.setMetadata(watcher);
	        spawned.sendPacket(player);
	        test.sendPacket(player);
	}

}
