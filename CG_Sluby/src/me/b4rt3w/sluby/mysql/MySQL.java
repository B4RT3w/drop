package me.b4rt3w.sluby.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import me.b4rt3w.sluby.Main;

import org.apache.commons.lang.Validate;

public class MySQL {

	@Data
	@AllArgsConstructor
	public static class Credentials {

		private String user, pass;
	}

	@Getter
	private MySQLHandler handle;
	private Connection connection;
	private Credentials credentials;
	private String host, database;
	private int port;
	


	public MySQL() {
		String user = Main.getCfg().getConfig().getString("settings.mysql.user");
		String pass = Main.getCfg().getConfig().getString("settings.mysql.password");
		this.handle = new MySQLHandler(this);
		this.credentials = new Credentials(user, pass);
		this.host = Main.getCfg().getConfig().getString("settings.mysql.host");
		this.port = Main.getCfg().getConfig().getInt("settings.mysql.port");
		this.database = Main.getCfg().getConfig().getString("settings.mysql.database");

	}

	PreparedStatement prepareStatement(String query) {
		try {
			return this.connection.prepareStatement(query);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			this.connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/"
					+ this.database + "?autoReconnect=true", this.credentials.getUser(), this.credentials.getPass());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	void callResultFuture(String query, MySQLFuture future) {
		Validate.notNull(future);
		Validate.notNull(query);
		this.reconnect();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = this.connection.createStatement();
			resultSet = statement.executeQuery(query);
			future.call(resultSet);
		} catch(Exception e) {
			future.exception(e);
		} finally {
			try {
				if(statement != null) statement.close();
				if(resultSet != null) resultSet.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}


	void execute(String query) {
		Validate.notNull(query);
		this.reconnect();
		Statement statement = null;
		try {
			statement = this.connection.createStatement();
			statement.execute(query);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(statement != null) statement.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void reconnect() {
		try {
			if(this.connection == null || this.connection.isClosed()) this.connect();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}