package me.b4rt3w.sluby.mysql;

import java.sql.ResultSet;

public interface MySQLFuture {
	
	void call(ResultSet resultset) throws Exception;
	void exception(Throwable throwable);
	
	
	
}
