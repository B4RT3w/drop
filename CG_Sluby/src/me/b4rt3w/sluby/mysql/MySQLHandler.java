package me.b4rt3w.sluby.mysql;

import java.sql.ResultSet;

import lombok.AllArgsConstructor;
import me.b4rt3w.sluby.Main;
import me.b4rt3w.sluby.MarriagePlayer;
import me.b4rt3w.sluby.pool.ThreadPool;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

@AllArgsConstructor
public class MySQLHandler {

	private static final String TABLE_NAME = "sluby_gracze";
	private static final String CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS `" 
			+ TABLE_NAME + "` (`nick` varchar(16) NOT NULL,"
			+ "`partner` varchar(16) default null, `monety` int default null,"
			+ "PRIMARY KEY (nick));";
	private static final String UPDATE_PARTNER_NAME_QUERY = "UPDATE `" + TABLE_NAME +"` SET `partner`='%1$s' WHERE `nick`='%2$s';";
	private static final String INSERT_PLAYER_QUERY = "INSERT INTO `" + TABLE_NAME +"` VALUES ('%s', NULL, '1000');";
	private static final String LOAD_PLAYER_QUERY = "SELECT * FROM `" + TABLE_NAME + "` WHERE `nick`='%s';";
	private static final String GET_PLAYER_MONEY = "SELECT `monety` FROM `" + TABLE_NAME + "` WHERE `nick`='%s';";
	private MySQL mysql;
	
	public void createTablesAsync() {
		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), new Runnable() {
			@Override
			public void run() {
				mysql.execute(CREATE_TABLE_QUERY);
			}
		});
	}
	
	public void updatePartnerNameAsync(final String playerName, final String partnerName) {
		ThreadPool.run(new Runnable() {
			@Override
			public void run() {
				String formatted = String.format(UPDATE_PARTNER_NAME_QUERY, partnerName, playerName);
				mysql.execute(formatted);
			}
		});
	}
	
	public void getMoneyAsync(final Player player) {
		ThreadPool.run(new Runnable() {
			
			@Override
			public void run() {
				String formatted = String.format(GET_PLAYER_MONEY, player.getName());
				mysql.callResultFuture(formatted, new MySQLFuture() {
					
					@Override
					public void exception(Throwable throwable) {
						throwable.printStackTrace();
						
					}
					
					@Override
					public void call(ResultSet resultset) throws Exception {
						final Integer money = (Integer) resultset.getObject("monety");
						System.out.print("Gracz: " + player.getName() + " ma " + money + " monet");
						
					}
				});
			}
		});
	}
	
	public void loadPlayerAsync(final MarriagePlayer player) {
		ThreadPool.run(new Runnable() {
			@Override
			public void run() {
				String formatted = String.format(LOAD_PLAYER_QUERY, player.getName());
				mysql.callResultFuture(formatted, new MySQLFuture() {
					
					@Override
					public void exception(Throwable throwable) {
						throwable.printStackTrace();
					}
					
					@Override
					public void call(ResultSet resultset) throws Exception {
						if(resultset.next()) 
							player.getHandle().setPartnerNameWithoutBase(resultset.getString("partner"));
						 else 
							insertPlayer(player.getName());
					}
				});
			}
		});
	}
	
	private void insertPlayer(String playerName) {
		String formatted = String.format(INSERT_PLAYER_QUERY, playerName);
		mysql.execute(formatted);
	}
	
	
}