package me.b4rt3w.sluby;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class MarriagePlayerHandler {
	
	private MarriagePlayer player;
	
	public void setPartnerName(String name) {
		this.setPartnerNameWithoutBase(name);
		Main.getMySQL().getHandle().updatePartnerNameAsync(player.getName(), name);
	}
	
	public void setPartnerNameWithoutBase(String name) {
		this.player.setPartnerName(name);
	}
	

	
	

}
