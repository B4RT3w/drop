package me.b4rt3w.sluby.managers;

import java.io.File;

import org.bukkit.Bukkit;

import me.b4rt3w.sluby.Main;

public class ConfigManager {
	
	public ConfigManager(Main main) {
		
	}
	
	public void createConfig() {
		if(!new File(Main.getInstance().getDataFolder(), "config.yml").exists()) {
			Bukkit.getConsoleSender().sendMessage("žežlCreating new config.yml...");
			Main.getCfg().getConfig().addDefault("settings.mysql.host", "localhost");
			Main.getCfg().getConfig().addDefault("settings.mysql.port", "3306");
			Main.getCfg().getConfig().addDefault("settings.mysql.database", "Main");
			Main.getCfg().getConfig().addDefault("settings.mysql.user", "root");
			Main.getCfg().getConfig().addDefault("settings.mysql.password", "pass");
			Main.getCfg().getConfig().options().copyDefaults(true);
			Main.getCfg().save();
			
		}
		if(!new File(Main.getInstance().getDataFolder(), "messages.yml").exists()) {
			Bukkit.getConsoleSender().sendMessage("žežlTworzenie nowego pliku: messages.yml...");
			Main.getMsgCfg().setMessage("prefix", "&d[Sluby] ");
			Main.getMsgCfg().save();
		}
	}
	
	public static String getPrefix() {
		return Main.getMsgCfg().getMessage("prefix");
	}
	
	public void loadMessages() {
		Main.getMsgCfg().fetch();
	}
	


}
