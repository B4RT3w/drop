package me.b4rt3w.sluby.managers;

import java.sql.SQLException;
import java.util.Arrays;

import me.b4rt3w.sluby.Main;
import me.b4rt3w.sluby.MarriagePlayer;
import me.b4rt3w.sluby.utils.ItemCreator;
import me.b4rt3w.sluby.utils.StringUtils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MenuManager {
	
	public static void openMenu(Player p) throws IllegalArgumentException, SQLException {
		p.openInventory(fillInv(Bukkit.getServer().createInventory(p, 9, "§dSluby"), p));
	}
	
	public static Inventory fillInv(Inventory inv, Player p) throws SQLException {
		ItemStack info;
		ItemStack ring;
		ItemStack divorse;
		
		MarriagePlayer player = Main.getPlayer(p.getName());
		
		if(player.isMarried()) {
			ring = new ItemCreator(Material.GOLD_NUGGET, 0 , StringUtils.fixColor("&4&l❤ &6&lObrączka &4&l❤"), Arrays.asList(StringUtils.fixColor("&6✦ &aJestes już w związku z &d" + player.getPartnerName()), StringUtils.fixColor("&6۩ &cAby ożenić się z inną osobą rozwiedź się."), StringUtils.fixColor("&6۩ &cPamiętaj że jeżeli się rozwiedziesz i będziesz chciał się ożenić, będziesz musiał poniesć koszty obrączki.")));
			info = new ItemCreator(Material.BOOK, 0, StringUtils.fixColor("&4&l❤ &d&lStatystyki &4&l❤"), Arrays.asList(StringUtils.fixColor("&6✦ &bW zwiazku: &aTak &a&l✔"), StringUtils.fixColor("&6✦ &bPartner: &d" + player.getPartnerName())));
		} else {
			ring = new ItemCreator(Material.GOLD_NUGGET, 0 , StringUtils.fixColor("&4&l❤ &6&lObraczka &4&l❤"), Arrays.asList(StringUtils.fixColor("&6✦ &bKliknij aby kupic obracze"),StringUtils.fixColor("&e✪  &cKoszt: &b1000 monet") ,StringUtils.fixColor("&6۩ &cPamietaj że jeżeli kupisz obrączkę musisz podać nick osoby z którą chcesz się ożenić!")));
			info = new ItemCreator(Material.BOOK, 0, StringUtils.fixColor("&4&l❤ &d&lStatystyki &4&l❤"), Arrays.asList(StringUtils.fixColor("&6✦ &bW zwiazku: &cNie &4&l✖"), StringUtils.fixColor("&6✦ &bJezeli chcesz się ozenic kup obraczke i oswiadcz sie komus.")));
		}
		
		
		inv.setItem(0, ring);
		inv.setItem(4, info);
		return inv;
	}

}
