package me.b4rt3w.sluby.listeners;

import me.b4rt3w.sluby.Main;
import me.b4rt3w.sluby.MarriagePlayer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener{
	

	
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		MarriagePlayer player = new MarriagePlayer(event.getPlayer().getName());
		Main.getMySQL().getHandle().loadPlayerAsync(player);
		Main.addPlayer(player);
	}
	
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Main.removePlayer(event.getPlayer().getName());
	}
	
	@EventHandler
	public void onInteract(InventoryClickEvent event) {
		if(event.getInventory().getName().startsWith("�dSluby")) {
			event.setCancelled(true);
		}
	}
	


}
