package me.b4rt3w.sluby;

import java.beans.ConstructorProperties;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public class MarriagePlayer {
	
	@Getter
	private MarriagePlayerHandler handle;
	@Getter
	private String name;
	@Getter
	private int money;
	@Getter
	@Setter(AccessLevel.PACKAGE)
	private String partnerName;
	
	@ConstructorProperties("name")
	public MarriagePlayer(String name) {
		this.handle = new MarriagePlayerHandler(this);
		this.name = name;
		
	}
	
	public boolean isMarried() {
		return this.partnerName != null;
	}
	
	

}
